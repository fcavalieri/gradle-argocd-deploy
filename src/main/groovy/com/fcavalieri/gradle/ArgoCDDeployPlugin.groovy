package com.fcavalieri.gradle

import groovy.transform.Internal
import org.ajoberstar.grgit.DiffEntry
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.bundling.Zip
import groovy.xml.*

import java.nio.file.Paths
import java.nio.file.Files
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import com.fcavalieri.gradle.GitLabCredentialsExtension

import org.ajoberstar.grgit.Grgit
import org.ajoberstar.grgit.Credentials
import com.fcavalieri.gradle.GitLabCredentialsExtension
import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.Yaml

import java.util.regex.Pattern
import java.util.stream.Collectors

class ArgoCDDeployPlugin implements Plugin<Project> {
    void apply(Project project) {
        //project.tasks.register("deployArgoCD", DeployArgoCDTask)
    }
}

class ArgoCDDeployTask extends DefaultTask {
    @Input
    String repositoryName

    @Input
    String repositoryUrl

    @Input
    ArrayList<String> patchedFiles = new ArrayList<>()

    @Input
    String imageName

    @Input
    String imageVersion

    @Input
    boolean dryRun = false

    @TaskAction
    void deployArgoCD() {
        DumperOptions options = new DumperOptions()
        options.setIndent(2)
        options.setPrettyFlow(true)
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK)
        Yaml yaml = new Yaml(options)

        GitLabCredentialsExtension credentials = new GitLabCredentialsExtension(project)
        credentials.read()

        new File("$project.buildDir/$repositoryName").deleteDir()
        logger.quiet("Deploying $imageName version $imageVersion to $repositoryUrl")
        def grgit = Grgit.clone(dir: "$project.buildDir/$repositoryName", uri: repositoryUrl, credentials: new Credentials("gradle", credentials.getValue()))

        def repoRoot = new File("$project.buildDir/$repositoryName")

        if (patchedFiles == null || patchedFiles.isEmpty()) {
            repoRoot.eachFileRecurse {it -> if (it.toString().toLowerCase().endsWith(".yml") || it.toString().toLowerCase().endsWith(".yaml")) patchedFiles.add(repoRoot.relativePath(it).toString())}
            logger.quiet("Considering the following files for patching:")
            for (def patchedFile: patchedFiles) {
                logger.quiet(patchedFile.toString())
            }
        }

        for (def patchedFile: patchedFiles) {
            def modified = false
            InputStream input = new FileInputStream(new File("$project.buildDir/$repositoryName/$patchedFile"))
            def yamlData = yaml.loadAll(input)
            def newYamlData = new ArrayList<Object>()
            if (Paths.get(patchedFile).getFileName().toString().toLowerCase().equals("kustomization.yaml")) {
                for (def yamlObject : yamlData) {
                    newYamlData.add(yamlObject)
                    List<Map<String, Object>> imageData = (yamlObject as Map<String, Object>).get("images") as List<Map<String, Object>>
                    List<Map<String, Object>> matchingObjs = imageData
                            .stream()
                            .filter(i -> imageName.equals(i.get("name")))
                            .collect(Collectors.toList())
                    if (matchingObjs !=null && !matchingObjs.isEmpty()) {
                        matchingObjs
                            .stream()
                            .forEach(i -> i.put("newTag", imageVersion))
                        modified = true
                    }
                }
            } else {
                for (def yamlObject : yamlData) {
                    newYamlData.add(yamlObject)
                    if (yamlObject instanceof Map) {
                        def subModified = processObject(yamlObject)
                        modified = modified || subModified
                    } else if (yamlObject instanceof List) {
                        def subModified = processList(yamlObject)
                        modified = modified || subModified
                    }
                }
            }
            if (modified) {
                FileWriter writer = new FileWriter(new File("$project.buildDir/$repositoryName/$patchedFile"))
                yaml.dumpAll(newYamlData.iterator(), writer)
            }
        }
        grgit.add(patterns: patchedFiles)
        grgit.commit(message: "Update $imageName to version $imageVersion")
        if (dryRun) {
            for (def patchedFile: patchedFiles) {
                logger.quiet(new File("$project.buildDir/$repositoryName/$patchedFile").text)
            }
        } else {
            grgit.push()
        }
    }

    boolean processObject(Map<String, Object> yamlObject) {
        if (yamlObject == null)
            return false
        boolean modified = false
        for (Map.Entry<String, Object> yamlEntry: yamlObject.entrySet()) {
            if (yamlEntry.getValue() instanceof Map) {
                def subModified = processObject(yamlEntry.getValue() as Map<String, Object>)
                modified = modified || subModified
            } else if (yamlEntry.getValue() instanceof List) {
                def subModified = processList(yamlEntry.getValue() as ArrayList<Object>)
                modified = modified || subModified
            } else if (yamlEntry.getValue() instanceof String) {
                def newString = processString(yamlEntry.getValue() as String)
                if (!newString.equals(yamlEntry.getValue())) {
                    yamlObject.put(yamlEntry.getKey(), newString)
                    modified = true
                }
            }
        }
        return modified
    }

    boolean processList(ArrayList<Object> yamlList) {
        if (yamlList == null)
            return
        boolean modified = false
        for (def i = 0; i < yamlList.size(); ++i) {
            if (yamlList.get(i) instanceof Map) {
                def subModified = processObject(yamlList.get(i) as Map<String, Object>)
                modified = modified || subModified
            } else if (yamlList.get(i) instanceof List) {
                def subModified = processList(yamlList.get(i) as ArrayList<Object>)
                modified = modified || subModified
            } else if (yamlList.get(i) instanceof String) {
                def newString = processString(yamlList.get(i) as String)
                if (!newString.equals(yamlList.get(i))) {
                    yamlList.set(i, newString)
                    modified = true
                }
            }
        }
        return modified
    }

    String processString(String value) {
        if (value == null)
            return value
        final Pattern tagPattern = Pattern.compile("^[a-zA-Z0-9_][a-zA-Z0-9_.\\-]{0,127}")
        if (imageName.equals(value)) {
            return imageName + ":" + imageVersion
        } else if (value != null && value.startsWith(imageName + ":")) {
            def candidateTag = value.substring(imageName.length() + 1)
            if (tagPattern.matcher(candidateTag).matches()) {
                return imageName + ":" + imageVersion
            }
            return value
        } else {
          return value
        }
    }
}
